#include <iomanip>
#include <iostream>
#include <string>
#include <tuple>
#include <vector>

#define CL_USE_DEPRECATED_OPENCL_1_2_APIS
#include <CL/cl.h>

#include "common.h"

const std::vector<std::tuple<cl_platform_info, std::string>> platformInfos = {
	{CL_PLATFORM_VERSION, "Platform version"},
	{CL_PLATFORM_NAME, "Platform name"},
	{CL_PLATFORM_VENDOR, "Platform vendor"},
	{CL_PLATFORM_EXTENSIONS, "Platform extensions"} };

const std::vector<std::tuple<cl_device_info, std::string>> deviceInfos = {
	{CL_DEVICE_NAME, "Device name"},
	{CL_DEVICE_VERSION, "Device version"},
	{CL_DRIVER_VERSION, "Driver version"},
	{CL_DEVICE_OPENCL_C_VERSION, "OpenCL C version"},
	{CL_DEVICE_VENDOR, "Device vendor"} };

bool
createTestKernel(
	cl_context* context,
	cl_device_id* device,
	cl_program* program,
	cl_kernel* kernel)
{
	std::string code;
	// code +=  "#pragma OPENCL EXTENSION cl_intel_subgroups : enable";
	// code +=  "\n";
	code += "__kernel void vecAdd(\n";
	code += "        __global PRECISION* a,\n";
	code += "        __global PRECISION* b,\n";
	code += "        __global PRECISION* c)\n";
	code += "{\n";
	code += "    const size_t i = get_global_id(0);\n";
	code += "\n";
	code += "    c[i] = a[i] + b[i];\n";
	code += "}\n";
	const char* codes[] = { code.c_str() };
	const size_t size = code.length();
	const size_t sizes[] = { size };
	const std::string options("-w -Werror -DPRECISION=float");
	const std::string kernelName("vecAdd");
	cl_int status;
	cl_build_status buildStatus;

	*program = clCreateProgramWithSource(
		*context,
		1,
		codes,
		sizes,
		&status);
	OCL_CHECK(status)
		OCL_CHECK(clBuildProgram(
			*program,
			1,
			device,
			options.c_str(),
			nullptr,
			nullptr))
	OCL_CHECK(clGetProgramBuildInfo(
		*program,
		*device,
		CL_PROGRAM_BUILD_STATUS,
		sizeof(cl_build_status),
		&buildStatus,
		nullptr))

	if (buildStatus == CL_BUILD_SUCCESS)
	{
		*kernel = clCreateKernel(
			*program,
			kernelName.c_str(),
			&status);
		OCL_CHECK(status)

			return (status == CL_SUCCESS);
	}
	else {
		return false;
	}
}

bool
executeTestKernel(
	cl_context* context,
	cl_device_id* device,
	cl_kernel* kernel,
	size_t maxWorkGroupSize)
{
	cl_int status;
	cl_bool unifiedMemory;
	size_t local, global, size;
	cl_mem a, b, c;
	cl_command_queue commandQueue;
	bool successfullyExecuted;

	OCL_CHECK(clGetDeviceInfo(
		*device,
		CL_DEVICE_HOST_UNIFIED_MEMORY,
		sizeof(cl_bool),
		&unifiedMemory,
		nullptr))
	local = maxWorkGroupSize;
	global = 1024 * local;
	size = global * sizeof(PRECISION);
	a = clCreateBuffer(*context, unifiedMemory ? (CL_MEM_READ_ONLY | CL_MEM_ALLOC_HOST_PTR) : CL_MEM_READ_ONLY, size, nullptr, &status);
	OCL_CHECK(status)
	successfullyExecuted = (status == CL_SUCCESS);
	b = clCreateBuffer(*context, unifiedMemory ? (CL_MEM_READ_ONLY | CL_MEM_ALLOC_HOST_PTR) : CL_MEM_READ_ONLY, size, nullptr, &status);
	OCL_CHECK(status)
	successfullyExecuted &= (status == CL_SUCCESS);
	c = clCreateBuffer(*context, unifiedMemory ? (CL_MEM_WRITE_ONLY | CL_MEM_ALLOC_HOST_PTR) : CL_MEM_WRITE_ONLY, size, nullptr, &status);
	OCL_CHECK(status)
	successfullyExecuted &= (status == CL_SUCCESS);
	commandQueue = clCreateCommandQueue(*context, *device, 0, &status);
	OCL_CHECK(status)
	successfullyExecuted &= (status == CL_SUCCESS);

	status = clSetKernelArg(*kernel, 0, sizeof(cl_mem), &a);
	OCL_CHECK(status)
	successfullyExecuted &= (status == CL_SUCCESS);
	status = clSetKernelArg(*kernel, 1, sizeof(cl_mem), &b);
	OCL_CHECK(status)
	successfullyExecuted &= (status == CL_SUCCESS);
	status = clSetKernelArg(*kernel, 2, sizeof(cl_mem), &c);
	OCL_CHECK(status)
	successfullyExecuted &= (status == CL_SUCCESS);

	status = clEnqueueNDRangeKernel(commandQueue, *kernel, 1, nullptr, &global, &local, 0, nullptr, nullptr);
	OCL_CHECK(status)
	successfullyExecuted &= (status == CL_SUCCESS);
	status = clFinish(commandQueue);
	OCL_CHECK(status)
	successfullyExecuted &= (status == CL_SUCCESS);

	status = clReleaseCommandQueue(commandQueue);
	OCL_CHECK(status)
	successfullyExecuted &= (status == CL_SUCCESS);

	return successfullyExecuted;
}

std::string
getPlatformInfo(
	const cl_platform_id platform,
	const cl_platform_info info)
{
	char* value;
	size_t valueSize;
	std::string result;

	OCL_CHECK(clGetPlatformInfo(
		platform,
		info,
		0,
		nullptr,
		&valueSize))
		value = new char[valueSize];
	OCL_CHECK(clGetPlatformInfo(
		platform,
		info,
		valueSize,
		value,
		nullptr))
	result = value;

	delete[] value;

	return result;
}

template <typename T>
void
getPlatformInfo(
	const cl_platform_id platform,
	const cl_platform_info info,
	T *result)
{
	OCL_CHECK(clGetPlatformInfo(
		platform,
		info,
		sizeof(T),
		result,
		nullptr))
}

std::string
getDeviceInfo(
	const cl_device_id device,
	const cl_device_info info)
{
	char* value;
	size_t valueSize;
	std::string result;

	OCL_CHECK(clGetDeviceInfo(
		device,
		info,
		0,
		nullptr,
		&valueSize))
	value = new char[valueSize];
	OCL_CHECK(clGetDeviceInfo(
		device,
		info,
		valueSize,
		value,
		nullptr))
	result = value;

	delete[] value;

	return result;
}

template <typename T>
void
getDeviceInfo(
	const cl_device_id device,
	const cl_device_info info,
	T *result)
{
	OCL_CHECK(clGetDeviceInfo(
		device,
		info,
		sizeof(T),
		result,
		nullptr))
}

std::string
getBuildLog(cl_program* program, cl_device_id* device)
{
	size_t size;
	char* buildLog;

	OCL_CHECK(clGetProgramBuildInfo(
		*program,
		*device,
		CL_PROGRAM_BUILD_LOG,
		0,
		nullptr,
		&size))
	buildLog = new char[size];
	OCL_CHECK(clGetProgramBuildInfo(
		*program,
		*device,
		CL_PROGRAM_BUILD_LOG,
		size,
		buildLog,
		nullptr))

	return std::string(buildLog);
}

int
main()
{
	cl_int status;
	cl_uint platformCount;
	cl_platform_id* platforms;
	cl_uint deviceCount;
	cl_device_id* devices;
	cl_context_properties contextProperties[3] = { CL_CONTEXT_PLATFORM, 0, 0 };
	cl_context context;
	cl_program program;
	cl_kernel kernel;
	std::string deviceExtensions;
	cl_device_type deviceType;
	size_t maxWorkGroupSize;
	cl_bool succesfullyBuilt;
	cl_bool succesfullyExecuted;
	size_t wavefrontSize;
#ifdef CL_KERNEL_MAX_SUB_GROUP_SIZE_FOR_NDRANGE
	size_t subgroupSize;
#endif

	cl_uint varUint;
	cl_ulong varUlong;

	// Get all platforms
	clGetPlatformIDs(0, nullptr, &platformCount);
	platforms = new cl_platform_id[platformCount];
	clGetPlatformIDs(platformCount, platforms, nullptr);

	std::cout << "Number of platforms: " << platformCount << std::endl;

	// For each platform print information
	for (unsigned int i = 0; i < platformCount; i++)
	{
		std::cout << std::endl << "Platform " << i + 1 << "/" << platformCount << ":" << std::endl;
		for (auto const &platformInfo : platformInfos)
			std::cout << std::string(1, 32) << std::left << std::setw(22) << std::get<1>(platformInfo) << getPlatformInfo(platforms[i], std::get<0>(platformInfo)) << std::endl;

		// CL_PLATFORM_HOST_TIMER_RESOLUTION
#if defined(CL_PLATFORM_HOST_TIMER_RESOLUTION)
		getPlatformInfo<cl_ulong>(platforms[i], CL_PLATFORM_HOST_TIMER_RESOLUTION, &varUlong);
		std::cout << std::string(1, 32) << std::left << std::setw(22) << "Host timer resolution" << varUlong << " ns" << std::endl;
#endif

		// Get all devices of the current platform
		OCL_CHECK(clGetDeviceIDs(
			platforms[i],
			CL_DEVICE_TYPE_ALL,
			0,
			nullptr,
			&deviceCount))
		devices = new cl_device_id[deviceCount];
		OCL_CHECK(clGetDeviceIDs(
			platforms[i],
			CL_DEVICE_TYPE_ALL,
			deviceCount,
			devices,
			nullptr))

		std::cout << " Number of devices: " << deviceCount << std::endl;

		// Create context for the devices of the current platform
		contextProperties[1] = (cl_context_properties)platforms[i];
		context = clCreateContext(
			contextProperties,
			deviceCount,
			devices,
			nullptr,
			nullptr,
			&status);
		OCL_CHECK(status)

		// For each device print information
		for (unsigned int j = 0; j < deviceCount; j++) {
			std::cout << " Device " << j + 1 << "/" << deviceCount << " of platform " << i + 1 << "/" << platformCount << ":" << std::endl;

			// Print device infos
			std::cout << "  Device infos:" << std::endl;

			// deviceInfos
			for (auto const &deviceInfo : deviceInfos)
				std::cout << std::string(3, 32) << std::left << std::setw(36) << std::get<1>(deviceInfo) << getDeviceInfo(devices[j], std::get<0>(deviceInfo)) << std::endl;

			// CL_DEVICE_VENDOR_ID
			getDeviceInfo<cl_uint>(devices[j], CL_DEVICE_VENDOR_ID, &varUint);
			std::cout << std::string(3, 32) << std::left << std::setw(36) << std::hex << "Device vendor ID" << "0x" << varUint << std::dec << std::endl;

			// CL_DEVICE_EXTENSIONS
			deviceExtensions = getDeviceInfo(devices[j], CL_DEVICE_EXTENSIONS);
			std::cout << std::string(3, 32) << std::left << std::setw(36) << "Device extensions" << deviceExtensions << std::endl;

			// CL_DEVICE_TYPE
			OCL_CHECK(clGetDeviceInfo(
				devices[j],
				CL_DEVICE_TYPE,
				sizeof(cl_device_type),
				&deviceType,
				nullptr))
			std::cout << std::string(3, 32) << std::left << std::setw(36) << "Device type" << ((deviceType & CL_DEVICE_TYPE_CPU) ? "CPU " : "")
				<< ((deviceType & CL_DEVICE_TYPE_GPU) ? "GPU " : "")
				<< ((deviceType & CL_DEVICE_TYPE_ACCELERATOR) ? "accelerator " : "")
				<< ((deviceType & CL_DEVICE_TYPE_DEFAULT) ? "default" : "") << std::endl;

			// CL_DEVICE_MAX_WORK_GROUP_SIZE
			getDeviceInfo<size_t>(devices[j], CL_DEVICE_MAX_WORK_GROUP_SIZE, &maxWorkGroupSize);
			std::cout << std::string(3, 32) << std::left << std::setw(36) << "Maximum work group size" << maxWorkGroupSize << std::endl;

			// CL_DEVICE_MAX_COMPUTE_UNITS
			getDeviceInfo<cl_uint>(devices[j], CL_DEVICE_MAX_COMPUTE_UNITS, &varUint);
			std::cout << std::string(3, 32) << std::left << std::setw(36) << "Parallel compute units" << varUint << " (CPU: #logical cores, Intel: #EUs, NVIDIA: #SMs)" << std::endl;

			// CL_DEVICE_GLOBAL_MEM_SIZE
			getDeviceInfo<cl_ulong>(devices[j], CL_DEVICE_GLOBAL_MEM_SIZE, &varUlong);
			std::cout << std::string(3, 32) << std::left << std::setw(36) << "Global memory size" << ((double)varUlong / (double)(1 << 30)) << " GB" << std::endl;

			// Print kernel infos
			std::cout << std::string(2, 32) << "Program/kernel/work group infos:" << std::endl;

			succesfullyBuilt = createTestKernel(&context, &devices[j], &program, &kernel);
			std::cout << std::string(3, 32) << std::left << std::setw(36) << "Test program successfully built" << (succesfullyBuilt ? "true" : "false") << std::endl;

			if (succesfullyBuilt)
			{
				succesfullyExecuted = executeTestKernel(&context, &devices[j], &kernel, maxWorkGroupSize);
				std::cout << std::string(3, 32) << std::left << std::setw(36) << "Test kernel successfully executed" << (succesfullyExecuted ? "true" : "false") << std::endl;

				if (succesfullyExecuted)
				{
					// Print wavefront size
					OCL_CHECK(clGetKernelWorkGroupInfo(
						kernel,
						devices[j],
						CL_KERNEL_PREFERRED_WORK_GROUP_SIZE_MULTIPLE,
						sizeof(size_t),
						&wavefrontSize,
						nullptr));
					std::cout << std::string(3, 32) << std::left << std::setw(36) << "Preferred work group size multiple" << wavefrontSize << " (CPU: compiler-/kernel-/vector-dependent, AMD: wavefront size, Intel: ToDo, NVIDIA: warp size)" << std::endl;

#ifdef CL_KERNEL_MAX_SUB_GROUP_SIZE_FOR_NDRANGE
					if (deviceExtensions.find("cl_khr_subgroups") != std::string::npos || deviceExtensions.find("cl_intel_subgroups") != std::string::npos)
					{
						// Print subgroup size
						OCL_CHECK(clGetKernelSubGroupInfo(
							kernel,
							devices[j],
							CL_KERNEL_MAX_SUB_GROUP_SIZE_FOR_NDRANGE,
							// 1 * sizeof(size_t)
							sizeof(size_t),
							// Parallel setup dim(maxWorkGroupSize, 1, 1)
							&maxWorkGroupSize,
							sizeof(size_t),
							&subgroupSize,
							nullptr));
						std::cout << std::string(3, 32) << std::left << std::setw(36) << "Maximum subgroup size" << subgroupSize << " elements" << std::endl;
					}
#endif

					OCL_CHECK(clReleaseKernel(kernel))
				}
			}
			else {
				std::cerr << "-- !!! The following OpenCL build error occurred !!! --" << std::endl;
				std::cerr << getBuildLog(&program, &devices[j]);
				std::cerr << "-------------------------------------------------------" << std::endl;
			}

			OCL_CHECK(clReleaseProgram(program))
		}

		OCL_CHECK(clReleaseContext(context))
			delete[] devices;
	}

	delete[] platforms;

	return 0;
}

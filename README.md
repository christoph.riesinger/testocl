# testocl

testocl is a simple command line program to determine basic OpenCL capabilities and properties of a system.
The major goal of testocl is portability, thus it is not limited to one particular operating system, vendor, type of computing device, or OpenCL version.
That's in the spirit of OpenCL.

Features
========
testocl prints OpenCL-related information to the console
- Iteration through all platforms of system and all devices of each platform
- Platform- and device-specific information such as driver versions, computing device types, and memory sizes
- Checks if OpenCL kernels are correctly compiled and executed during runtime

Requirements
============
Since testocl focuses on portability, the set of prerequisites is small:
- A C++ compiler
- CMake (at least version 3.7 for OpenCL detection)
- and of course: An OpenCL SDK for compilation and an OpenCL runtime for execution

Build
=====
1. Install CMake and Git
  * Linux
    * CentOS: `sudo yum install -y cmake git`
    * Fedora: `sudo dnf install -y cmake git`
    * Ubuntu: `sudo apt-get install cmake git`
  * MacOS: ToDo
  * Windows
    * [CMake](https://cmake.org/download/)
    * [Git](https://git-scm.com/download/win)
2. Clone the repository via `git clone https://gitlab.com/christoph.riesinger/testocl.git`
3. Create a subfolder for the build process (in the following called _build_) and change into it
4. Configure CMake.
  There are no user-defined variables provided by testocl's CMake scripts so there are also no variables passable to CMake.

  Remark: `cmake -G` gives a list of generators on the current system .
  An example how to use such a generator is given for Windows.
  * Linux: `cmake ..`
  * MacOS: ToDo
  * Windows: `cmake .. -G "Visual Studio 15 2017 Win64"` generates project and solution files for Visual Studio 2017 with a Win64 target
5. Compile
  * Linux: `make -j$(nproc)`
  * MacOS: ToDo
  * Windows: Open the generated solution file with Visual Studio and compile the testocl project

Usage
=====
There are no parameters to testocl. Simply run it from your command line interpreter of choice.

Troubleshooting
=====
* _Problem_: The local OpenCL installation is not detected by CMake

  _Solution_: The [CMake script to detect the OpenCL environment](https://github.com/Kitware/CMake/blob/master/Modules/FindOpenCL.cmake) provides two cachable variables `OpenCL_INCLUDE_DIR` and `OpenCL_LIBRARY` to manually specify the local OpenCL installation.
  An example call of CMake could look like `cmake .. -DOpenCL_INCLUDE_DIR=<path to the OpenCL installation directory>/include -DOpenCL_LIBRARY=<path to the OpenCL library directory>/libOpenCL.so` for a Linux environment.
